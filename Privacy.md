Text Edit Revived - Privacy Policy
==================================

This privacy policy is required due to publication on _Google Play_:

_Text Edit Revivied_ does not collect any personal information.
It is is intended purely for editing of text files on the device's local storage.