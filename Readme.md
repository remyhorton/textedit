Text Editor for Android
=======================
_Text Edit Revivied_ is a fork of Paul Mach's [Text Edit][orig] which updates the classic text editor for modern devices, and is based on v1.5 of Paul's [source code][ghub] which was released back in 2011.
The classic version was made for Android 2.1 and is still the best Android-based text editor I have ever used, but due to complete removal of support for the _Menu Button_ and a substantial tightening-up of the security model used for document files, it is basically unusable on Android 10 devices.
This updated release brings the classic experience to modern devices.

Summary
-------
Simple notepad app to open, edit and save text files to and from the SD card.

* Open email attachments
* Change font size, type and color
* Delete/rename in file browser
* Email as text or attachment
* Search using "search" button

Licence
-------
As with the original this fork is licenced under [version 3 of the GPL][gpl3] and the [source code is available][srcs] from my Bitbucket account.


Contact
-------
The original application was by ``paulmach`` (at) ``gmail.com`` and this updated version is by ``remy.horton`` (at) ``gmail.com``.


[orig]: http://textedit.paulmach.com/
[ghub]: https://github.com/paulmach/Text-Edit-for-Android
[srcs]: http://www.rahorton.net
[gpl3]: https://www.gnu.org/licenses/gpl-3.0.en.html

